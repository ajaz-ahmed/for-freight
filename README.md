**A project that takes care of Deploying a simple flask application using different technologies**

---

## Tech Stack in Use

1. Application - Python based Flask
2. Containerization - Docker
3. Orchestration - Minikube
4. Testing - dgoss
5. IaC - Terraform
6. Config Mgmt - Ansible
7. CI Pipeline - Travis

---

## How to use

* Clone the repo
* Associate the src within your Version control system
* Associate the repo in to travisCI
* Travis should build and test the App.
* The Travis YAML Takes care
    * Build
    * Test using Goss
    * Push is not included as it requires a repo to be created
* Go to the for-freight directory
* Assuming Vagrant is pre-installed on the machine
* Trigger the vagrant up command
* Vagrant will take care of the following
    * Creates a VM with pre-defined network, name, ports etc
    * Triggers a Ansible Playbook inside the created VM
* Ansible Playbook takes care of the following
    * Docker install and start
    * Kubectl install and start
    * Minikube install and start
    * Clone the repo inside the VM for the TF templates
    * Terraform install
    * Terraform Apply

---

## Please Note

*I do not have a powerful machine to create a VM on top of the host machine, however from the understanding of my knowledge the above modules should execute fine*
