provider "kubernetes" {
    config_context_cluster = "minikube"
}

resource "kubernetes_deployment" "this" {
  metadata {
    name = "${var.image_name}"
    labels = {
        app = "${var.image_name}"
    }
  }

  spec {
    replicas = var.number-of-replicas
    
    selector {
        match_labels = {
          "app" = "${var.image_name}"
        }
    }

    template {
      metadata {
        labels = {
          "app" = "${var.image_name}"
        }
      }

      spec {
          container {
            image = "${var.repo_name}/${var.image_name}:latest"
            name = "${var.image_name}"

            resources {
              limits = {
                cpu = "1"
                memory = "512Mi"
              }
            }
          }
      }
    }
  }
}

resource "kubernetes_service" "this" {
    metadata {
      name = "${var.image_name}-svc"
    }

    spec {
      selector = {
        "app" = "${var.image_name}"
      }
      port {
        port = var.port_to_use
        target_port = var.port_to_use
        node_port = var.node_port_to_use
      }
      type = "NodePort"
    }
}