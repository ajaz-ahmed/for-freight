- hosts: all
  become: yes
  become_method: sudo
  tasks:

    - name: install system updates
      yum: 
        name: '*'
        state: latest 
        update_cache: "yes"

    - name: Disable SELinux
      selinux:
        state: disabled

    - name: disable swap
      shell: /usr/sbin/swapoff -a

    - name: disable swap permanently
      lineinfile:
        path: /etc/fstab
        regexp: '^[^#].*swap.*$'
        state: absent

    - name: delete swapfile
      file:
        path: /swapfile
        state: absent

    - name: install prerequisites
      yum:
        name: [ vim, socat, device-mapper-persistent-data, lvm2, telnet, tcpdump, conntrack-tools, bind-utils ]

    - name: download minikube 
      get_url:
        url: https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm
        dest: /var/tmp/minikube-latest.x86_64.rpm
        mode: '0644'

    - name: install minikube
      yum:
        name: /var/tmp/minikube-latest.x86_64.rpm

    - name: delete minikube
      file:
        path: /var/tmp/minikube-latest.x86_64.rpm
        state: absent

    - name: add docker repository
      yum_repository:
        name: docker-ce-stable
        description: Docker CE Stable - $basearch
        baseurl: https://download.docker.com/linux/centos/7/$basearch/stable
        gpgcheck: yes
        gpgkey: https://download.docker.com/linux/centos/gpg

    - name: install docker-ce
      yum:
        name: docker-ce

    - name: start and enable docker service
      service:
        name: docker
        enabled: yes
        state: started

    - name: Add vagrant user to docker group
      user:
        name: vagrant
        groups: docker
        append: yes

    - name: set bridge-nf-call-iptables = 1
      lineinfile:
        path: /etc/sysctl.conf
        line: net.bridge.bridge-nf-call-iptables = 1

    - name: apply sysctl config
      shell: sysctl -p

    - name: start minikube
      shell: minikube start --vm-driver=none

    - name: Add service-node-port-range parameter to minikube config file
      lineinfile:
        state: present
        path: /etc/kubernetes/manifests/kube-apiserver.yaml
        line: '    - --service-node-port-range=1024-65535'
        insertafter: '^.*service-account-key-file.*$'

    - name: start and enable kubelet service
      service:
        name: kubelet
        enabled: yes
        state: started
    
    - name: create dir /home/vagrant/bin
      file:
        path: /home/vagrant/bin
        state: directory

    - name: add kubernetes repository
      yum_repository:
        name: Kubernetes
        description: Kubernetes
        baseurl: https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
        gpgcheck: yes
        gpgkey: https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg

    - name: install kubectl
      yum:
        name: kubectl

    - name: copy kubectl conf
      copy:
        src: /root/.kube
        dest: /home/vagrant/
        remote_src: yes
        owner: vagrant
        group: vagrant

    - name: fix kubectl conf
      replace:
        path: /home/vagrant/.kube/config
        regexp: '/root'
        replace: '/home/vagrant'

    - name: enable kubectl autocompletion
      lineinfile:
        path: /home/vagrant/.bashrc
        line: source <(kubectl completion bash)
        insertafter: EOF

    - name: copy minikube conf
      copy:
        src: /root/.minikube
        dest: /home/vagrant/
        remote_src: yes
        owner: vagrant
        group: vagrant

    - name: create /vagrant directory
      file:
        path: /home/vagrant/
        state: directory
    
    - name: Clone my repo with separate git directory
      git:
        repo: https://ajaz-ahmed@bitbucket.org/ajaz-ahmed/for-freight.git
        dest: /home/vagrant/
    
    - name: terraform install
      unarchive:
        src: https://releases.hashicorp.com/terraform/1.0.8/terraform_1.0.8_linux_amd64.zip
        dest: /usr/bin
        remote_src: True

    - name: "Initialize and apply Terraform"
      terraform:
        project_path: "/home/vagrant/for-freight/vagrant"
        state: "present"
        force_init: true
      tags: apply-terraform